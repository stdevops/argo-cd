# Setup a Sample Service

For the purpose of this guide, we will setup an httpbin service in the cluster and proxy it.

```
kubectl apply -f https://bit.ly/k8s-httpbin
service/httpbin created
deployment.apps/httpbin created
```

# Create an Ingress rule to proxy the httpbin service we just created:


```
echo '
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: demo
  annotations:
    kubernetes.io/ingress.class: kong
spec:
  rules:
  - host: a53d89d249ffe4bebbe7bef3f7d470a5-9c56568e82629c70.elb.us-east-2.amazonaws.com
    http:
      paths:
      - path: /
        backend:
          serviceName: httpbin
          servicePort: 80
' | kubectl apply -f -
ingress.extensions/demo created
```



#  Test the Ingress rule:
```
curl -i a53d89d249ffe4bebbe7bef3f7d470a5-9c56568e82629c70.elb.us-east-2.amazonaws.com/status/200

```
